const Bicicleta = require('../../models/bicicleta');

// Obtener todas las bicicletas
exports.bicicletaList = (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

// Crear bicicleta
exports.bicicletaCreatePost = (req, res) => {
    const bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng]
    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

// Update bicicleta
exports.bicicletaUpdatePost = (req, res) => {
    const bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng]

    res.status(200).json({
        bicicleta: bici
    });
}

// Delete bicicleta
exports.bicicletaDelete = (req, res) => {
    Bicicleta.removeById(req.body.id);
    
    res.status(200).send(`Bicicleta con id ${req.body.id} removida`);
}