var express = require('express');
var router = express.Router();

const bicicletaController = require('../controllers/bicicletaController');

router.get('/', bicicletaController.bicicletaList);
// Create
router.get('/create', bicicletaController.bicicletaCreateGet);
router.post('/create', bicicletaController.bicicletaCreatePost);
// Update
router.get('/:id/update', bicicletaController.bicicletaUpdateGet);
router.post('/:id/update', bicicletaController.bicicletaUpdatePost);
// Delete
router.post('/:id/delete', bicicletaController.bicicletaDelete);

module.exports = router;