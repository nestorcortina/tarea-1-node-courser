var express = require('express');
var router = express.Router();

const bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicletaList);
// Create
router.post('/create', bicicletaController.bicicletaCreatePost);
/// Update
router.post('/:id/update', bicicletaController.bicicletaUpdatePost);
// Delete
router.delete('/delete', bicicletaController.bicicletaDelete);

module.exports = router;